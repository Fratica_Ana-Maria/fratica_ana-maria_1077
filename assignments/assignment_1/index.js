
const FIRST_NAME = "Ana-Maria";
const LAST_NAME = "Fratica";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if ((typeof value=="number" && value>=Number.MAX_VALUE) || (typeof value=="number" && value<Number.MIN_VALUE)) return NaN;
    if(typeof value=="number") return parseInt(value);
    if(typeof value=="string") return parseInt(value);
    if(isNaN(value)==1) return NaN;
    if(value==Infinity || value==-Infinity) return NaN;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}


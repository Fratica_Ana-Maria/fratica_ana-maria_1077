
const FIRST_NAME = "Ana-Maria";
const LAST_NAME = "Frătică";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name, surname, salary) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    getDetails(){
        return `${this.name} ${this.surname} ${this.salary}`;
    }
}

class SoftwareEngineer extends Employee {
    constructor(name, surname, salary, experience='JUNIOR') {
        super(name, surname, salary);
        this.experience = experience;
    }
    applyBonus(){
        if(this.experience == 'MIDDLE'){
            return this.salary + 0.15*this.salary;
        }else if(this.experience == 'SENIOR'){
            return this.salary + 0.20*this.salary;
        }else return this.salary + 0.10*this.salary;
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}


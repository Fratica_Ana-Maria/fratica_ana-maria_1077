
const FIRST_NAME = "Frătică";
const LAST_NAME = "Ana-Maria";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
function initCaching( ) {
    var cache = {};

    cache.pageAccessCounter = function( el = 'home' ){
        el = el.toLowerCase();
        if (this[el] == null) this[el]= 0;
        this[el] = this[el]+1;
    }
    cache.getCache = function (){
        return this;
    }

    return cache;
}

 

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

